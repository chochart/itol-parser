#! /usr/bin/env python
#########################################################################################
#
#   colors_styles_templates.py - 
#
#########################################################################################
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, ##
## INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A       ##
## PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT  ##
## HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   ##
## OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      ##
## SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                              ##
#########################################################################################

import os
import sys

from itol.DefaultValues import DefaultValues 

class colors_styles_templates() :

    def __init__(self) :

        self.ANNOTATION_TYPE = 'TREE_COLORS'
        self.SEPARATOR = 'SPACE'
        self.DATA = ''

    def write_annotation(self, output) :

        if self.SEPARATOR == 'TAB' :
            sep = '\t'
        elif self.SEPARATOR == 'SPACE' :
            sep = ' '
        elif self.SEPARATOR == 'COMMA' : 
            sep = ','

        output_handle = open(output,'w')
        output_handle.write('\n'.join([
        self.ANNOTATION_TYPE,
        DefaultValues.MANDATORY,
        'SEPARATOR ' + self.SEPARATOR,
        DefaultValues.DATA,
        "DATA",
        "#NODE_ID TYPE COLOR LABEL_OR_STYLE SIZE_FACTOR",
        self.DATA
        ])+'\n')
