#! /usr/bin/env python
#########################################################################################
#
#   dataset_binary.py - 
#
#########################################################################################
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, ##
## INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A       ##
## PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT  ##
## HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   ##
## OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      ##
## SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                              ##
#########################################################################################

import os
import sys

from itol.DefaultValues import DefaultValues 

class dataset_binary() :

    def __init__(self) :

        self.ANNOTATION_TYPE = 'DATASET_BINARY'

        self.SEPARATOR = 'SPACE'
        self.DATASET_LABEL = 'label1'
        self.COLOR = '#ff0000'
        self.FIELD_SHAPES = '1'
        self.FIELD_LABELS = 'f1'

        self.FIELD_COLORS = '#ff0000'

        self.SHOW_INTERNAL = 0
        self.MARGIN = 0
        self.ALIGN_TO_LABELS = 1
        self.HEIGHT_FACTOR = 1
        self.SYMBOL_SPACING = 10
        self.SHOW_LABELS = 0
        #FIELD_SHAPES,2,4,5,1
        #FIELD_LABELS,f1,f2,f3,f4
        #FIELD_COLORS,#ff0000,#00ff00,#ffff00,#0000ff

        self.DATA = ''

    def write_annotation(self, output) :

        if self.SEPARATOR == 'TAB' :
            sep = '\t'
        elif self.SEPARATOR == 'SPACE' :
            sep = ' '
        elif self.SEPARATOR == 'COMMA' : 
            sep = ','

        LEGEND_TITLE = self.DATASET_LABEL
        LEGEND_SHAPES = self.FIELD_SHAPES
        LEGEND_COLORS = self.FIELD_COLORS
        LEGEND_LABELS = self.FIELD_LABELS

        output_handle = open(output,'w')
        output_handle.write('\n'.join([
        self.ANNOTATION_TYPE,
        DefaultValues.MANDATORY,
        'SEPARATOR ' + self.SEPARATOR,
        'DATASET_LABEL%s%s' % (sep,self.DATASET_LABEL),
        'COLOR%s%s' % (sep,self.COLOR),
        'FIELD_SHAPES%s%s' % (sep,self.FIELD_SHAPES),
        'FIELD_LABELS%s%s' % (sep,self.FIELD_LABELS),
        DefaultValues.OPTIONAL,
        'FIELD_COLORS%s%s' % (sep,self.FIELD_COLORS),
        'SHOW_INTERNAL%s%s' % (sep,self.SHOW_INTERNAL),
        'MARGIN%s%s' % (sep,self.MARGIN),
        'ALIGN_TO_LABELS%s%s' % (sep,self.ALIGN_TO_LABELS),
        'HEIGHT_FACTOR%s%s' % (sep,self.HEIGHT_FACTOR),
        'SYMBOL_SPACING%s%s' % (sep,self.SYMBOL_SPACING),
        'SHOW_LABELS%s%s' % (sep,self.SHOW_LABELS),
        'LEGEND_TITLE%s%s' % (sep,LEGEND_TITLE),
        'LEGEND_SHAPES%s%s' % (sep,LEGEND_SHAPES),
        'LEGEND_COLORS%s%s' % (sep,LEGEND_COLORS),
        'LEGEND_LABELS%s%s' % (sep,LEGEND_LABELS),
        DefaultValues.DATA,
        "DATA",
        "#node 9606 will have a filled circle, empty left triangle, nothing in the 3rd column and an empty rectangle",
        "#9606,1,0,-1,0",
        self.DATA
        ])+'\n')
