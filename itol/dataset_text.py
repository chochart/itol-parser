#! /usr/bin/env python
#########################################################################################
#
#   dataset_text.py - 
#
#########################################################################################
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, ##
## INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A       ##
## PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT  ##
## HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION   ##
## OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      ##
## SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                              ##
#########################################################################################

import os
import sys

from itol.DefaultValues import DefaultValues 

class dataset_text() :

    def __init__(self) :

        self.ANNOTATION_TYPE = 'DATASET_TEXT'
        self.SEPARATOR = 'SPACE'
        self.DATASET_LABEL = 'example text dataset'
        self.COLOR = '#ff0000'
        self.MARGIN = 0
        self.SHOW_INTERNAL = 0
        self.LABEL_ROTATION = 0
        self.STRAIGHT_LABELS = 0
        self.ALIGN_TO_TREE = 0
        self.SIZE_FACTOR = 1
        self.DATA = ''

    def write_annotation(self, output) :

        if self.SEPARATOR == 'TAB' :
            sep = '\t'
        elif self.SEPARATOR == 'SPACE' :
            sep = ' '
        elif self.SEPARATOR == 'COMMA' : 
            sep = ','

        output_handle = open(output,'w')
        output_handle.write('\n'.join([
        self.ANNOTATION_TYPE,
        DefaultValues.MANDATORY,
        'SEPARATOR ' + self.SEPARATOR,
        'DATASET_LABEL%s%s' % (sep,self.DATASET_LABEL),
        'COLOR%s%s' % (sep,self.COLOR),
        DefaultValues.OPTIONAL,
        'MARGIN%s%s' % (sep,self.MARGIN),
        'SHOW_INTERNAL%s%s' % (sep,self.SHOW_INTERNAL),
        'LABEL_ROTATION%s%s' % (sep,self.LABEL_ROTATION),
        'STRAIGHT_LABELS%s%s' % (sep,self.STRAIGHT_LABELS),
        'ALIGN_TO_TREE%s%s' % (sep,self.ALIGN_TO_TREE),
        'SIZE_FACTOR%s%s' % (sep,self.SIZE_FACTOR),
        DefaultValues.DATA,
        "DATA",
        "#ID,label,position,color,style,size_factor,rotation",
        self.DATA
        ])+'\n')
